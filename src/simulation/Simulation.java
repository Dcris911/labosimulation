package simulation;

import com.Entreprise;

public class Simulation {

	/**
	 * Cette classe représente l'application dans son ensemble.
	 */
	public static void main(String[] args) {
		Entreprise e = new Entreprise();
		Environnement environnement = new Environnement(e);
		FenetrePrincipale fenetre = new FenetrePrincipale(e);
		
		environnement.addPropertyChangeListener(fenetre);
		environnement.execute();
	}

}
