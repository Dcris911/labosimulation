package simulation;

import javax.swing.SwingWorker;
import com.Entreprise;
import com.util.Constante;

import static com.util.Constante.*;

public class Environnement extends SwingWorker<Object, String> {
	private boolean actif = true;
	private Entreprise entreprise;
	private int compteur;

	public Environnement(Entreprise e) {
		super();
		this.compteur = 0;
		this.entreprise = e;
	}
	
	@Override
	protected Object doInBackground() throws Exception {
		while(actif) {
				Thread.sleep(DELAI);
				entreprise.updateNoeuds();
				
				//Voir propertyChange(Event) de com.simulation.FenetrePrincipale
				firePropertyChange(Constante.NOM_PROPRIETE_CHANGER, null, "Tour numero " + Integer.toString(compteur++));
			}
		return null;
	}
	
}