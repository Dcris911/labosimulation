package simulation;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JFrame;

import com.Entreprise;

import static com.util.Constante.TITRE_FENETRE_FENETRE_PRINCIPALE;
import static com.util.Constante.DIMENSION_FENETRE_PRINCIPALE;
import static com.util.Constante.NOM_PROPRIETE_CHANGER;

public class FenetrePrincipale extends JFrame implements PropertyChangeListener {

	private static final long serialVersionUID = 1L;

	public FenetrePrincipale(Entreprise e) {
		PanneauPrincipal 	panneauPrincipal 	= new PanneauPrincipal(e);
		MenuFenetre 		menuFenetre 		= new MenuFenetre(e);
		
		add(panneauPrincipal);
		add(menuFenetre, BorderLayout.NORTH);
		
		setConfigurationFenetrePrincipale();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(NOM_PROPRIETE_CHANGER)) {
			repaint();
			System.out.println(evt.getNewValue());
		}
	}
	
	private void setConfigurationFenetrePrincipale() {
		// Faire en sorte que le X de la fen�tre ferme la fen�tre
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle(TITRE_FENETRE_FENETRE_PRINCIPALE);
		setSize(DIMENSION_FENETRE_PRINCIPALE);
		
		// Rendre la fen�tre visible
		setVisible(true);
		
		// Mettre la fen�tre au centre de l'�cran
		setLocationRelativeTo(null);
		
		// Emp�cher la redimension de la fen�tre
		setResizable(false);
	}
}
