package simulation;

import javax.swing.JFrame;


import static com.util.Constante.TITRE_FENETRE_FENETRE_STRATEGIE;
import static com.util.Constante.DIMENSION_FENETRE_STRATEGIE;

public class FenetreStrategie extends JFrame {

	private static final long serialVersionUID = 1L;
	private PanneauStrategie panneauStrategie;

	public FenetreStrategie() {
		this.panneauStrategie = new PanneauStrategie();
		add(panneauStrategie);
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle(TITRE_FENETRE_FENETRE_STRATEGIE);
		setSize(DIMENSION_FENETRE_STRATEGIE);
		setVisible(true);
		setLocationRelativeTo(null);
		setResizable(false);
	}
}
