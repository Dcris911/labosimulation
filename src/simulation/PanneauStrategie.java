package simulation;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

import com.strategie.ChoixStrategie;
import com.util.Constante;

public class PanneauStrategie extends JPanel {

	private static final long serialVersionUID = 1L;
	
	
	public PanneauStrategie() {

		ButtonGroup groupeBoutons = new ButtonGroup();
		JRadioButton strategie1 = new JRadioButton(Constante.STRATEGY_FIXE);
		JRadioButton strategie2 = new JRadioButton(Constante.STRATEGY_ALEATOIRE);	
		JButton boutonConfirmer = new JButton(Constante.BOUTON_CONFIRMER);

		boutonConfirmer.addActionListener((ActionEvent e) -> {
			ChoixStrategie.changerStragtegy(getSelectedButtonText(groupeBoutons));
			
			// Fermer la fen�tre du composant
			SwingUtilities.getWindowAncestor((Component) e.getSource()).dispose();
		});

		JButton boutonAnnuler = new JButton(Constante.BOUTON_ANNULER);

		boutonAnnuler.addActionListener((ActionEvent e) -> {
			// Fermer la fen�tre du composant
			SwingUtilities.getWindowAncestor((Component) e.getSource()).dispose();
		});
		
		groupeBoutons.add(strategie1);
		groupeBoutons.add(strategie2);		
		add(strategie1);
		add(strategie2);		
		add(boutonConfirmer);
		add(boutonAnnuler);
	}

	/**
	 * Retourne le bouton s�lectionn� dans un groupe de boutons.
	 * @param groupeBoutons
	 * @return le texte du bouton ou null
	 */
	private String getSelectedButtonText(ButtonGroup groupeBoutons) {
		for (Enumeration<AbstractButton> boutons = groupeBoutons.getElements(); boutons.hasMoreElements();) {
			AbstractButton bouton = boutons.nextElement();
			if (bouton.isSelected()) {
				return bouton.getText();
			}
		}
		return null;
	}

}
