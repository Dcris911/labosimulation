package simulation;

import java.awt.Graphics;

import javax.swing.JPanel;

import com.Entreprise;

public class PanneauPrincipal extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private Entreprise entreprise;

	public PanneauPrincipal(Entreprise e) {
		super();
		this.entreprise = e;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		entreprise.paint(g);
	}
}