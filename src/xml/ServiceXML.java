package xml;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.Noeud;
import com.composante.Avion;
import com.observateur.Observer;
import com.observateur.Subject;
import com.reseau.CheminReseau;
import com.strategie.ChoixStrategie;
import com.usine.Entrepot;
import com.usine.Usine;
import com.usine.UsineAile;
import com.usine.UsineAvion;
import com.usine.UsineMetal;
import com.usine.UsineMoteur;
import com.usine.UsineProductrice;
import com.util.Constante;
import com.util.PointE;
import com.util.VisuelUsine;

import xml.objet.XMLConfiguration;
import xml.objet.metadonne.XMLEntree;
import xml.objet.metadonne.XMLIcone;
import xml.objet.metadonne.XMLUsineMeta;
import xml.objet.simulation.XMLCheminSimulation;
import xml.objet.simulation.XMLUsineSimulation;

import static com.util.Constante.*;

public class ServiceXML {
	
	private LectureXMLXstream xml;
	private XMLConfiguration config;
	private Map<String, VisuelUsine> vu;
	
	public ServiceXML(String path) {
		super();
		this.xml = new LectureXMLXstream(path);
		this.config = xml.lectureXML();
		this.vu = getVisuelUsine();
	}

	public Map<String, VisuelUsine> getVisuelUsine() {
		List<XMLUsineMeta> liste = this.config.getMetadonnees().getUsine();
		Map<String, VisuelUsine> map = new HashMap<>();
		for (XMLUsineMeta uxml : liste) {
			Map<String, String> mapIcone = new HashMap<>();
			for (XMLIcone ixml : uxml.getIcones().getIcone()) {
				mapIcone.put(ixml.getType(), ixml.getPath());
			}
			map.put(uxml.getType(), new VisuelUsine(mapIcone.get(VIDE), 
					mapIcone.get(UNTIERS), mapIcone.get(DEUXTIERS), mapIcone.get(PLEIN)));
		}
		return map;
	}

	public int getTempsFabrication(String nomUsine) {
		List<XMLUsineMeta> liste = this.config.getMetadonnees().getUsine();
		for (XMLUsineMeta uxml : liste) {
			if(uxml.getType().equalsIgnoreCase(nomUsine)) {
				return uxml.getIntervalProduction();
			}
		}
		return -1;
	}

	public int getQuantite(String nomUsine, String typeComposante) {
		List<XMLUsineMeta> liste = this.config.getMetadonnees().getUsine();
		for (XMLUsineMeta uxml : liste) {
			if(uxml.getType().equalsIgnoreCase(nomUsine)) {
				for (XMLEntree i : uxml.getEntree()) {
					if(i.getType().equalsIgnoreCase(typeComposante)) {
						return Integer.parseInt(i.getQuantite());
					}
				}
			}
		}
		return -1;
	}

	public int getLimite(String nomEntrepot, String typeComposante) {
		List<XMLUsineMeta> liste = this.config.getMetadonnees().getUsine();
		for (XMLUsineMeta uxml : liste) {
			if(uxml.getType().equalsIgnoreCase(nomEntrepot)) {
				for (XMLEntree i : uxml.getEntree()) {
					if(i.getType().equalsIgnoreCase(typeComposante)) {
						return Integer.parseInt(i.getCapacite());
					}
				}
			}
		}
		return -1;
	}
	
	public List<Noeud> creerListeNoeud(ChoixStrategie cs) {
		 List<Noeud> noeuds = new ArrayList<>();
		 creerUsines(noeuds);
		 creerChemin(noeuds);
		 return noeuds;
	}

	private void creerUsines(List<Noeud> noeuds) {
		String key;
		Usine usine;
		HashSet<Observer> listeObserver = new HashSet<>();
		Subject sujet = null;
		for (XMLUsineSimulation uSim : config.getSimulation().getUsineSim()) {
			key = "";
			switch (uSim.getType()) {
			case Constante.NOM_USINE_METAL:
				key = NOM_USINE_METAL;
				usine = new UsineMetal(Integer.parseInt(uSim.getId()),
						new Point(Integer.parseInt(uSim.getX()), Integer.parseInt(uSim.getY())), 
						vu.get(key), getTempsFabrication(key));
				setUsine(usine, noeuds, listeObserver);
				break;
			case Constante.NOM_USINE_AILE:
				key = NOM_USINE_AILE;
				usine = new UsineAile(Integer.parseInt(uSim.getId()), 
						new Point(Integer.parseInt(uSim.getX()), Integer.parseInt(uSim.getY())), 
						vu.get(key), getTempsFabrication(key), getQuantite(key, METAL));
				setUsine(usine, noeuds, listeObserver);
				break;
			case Constante.NOM_USINE_MOTEUR:
				key = NOM_USINE_MOTEUR;
				usine = new UsineMoteur(Integer.parseInt(uSim.getId()), 
						new Point(Integer.parseInt(uSim.getX()), Integer.parseInt(uSim.getY())), 
						vu.get(key), getTempsFabrication(key), getQuantite(key, METAL));
				setUsine(usine, noeuds, listeObserver);
				break;
			case NOM_USINE_AVION:
				key = NOM_USINE_AVION;
				usine = new UsineAvion(Integer.parseInt(uSim.getId()), 
						new Point(Integer.parseInt(uSim.getX()), Integer.parseInt(uSim.getY())), 
						vu.get(key), getTempsFabrication(key), getQuantite(key, MOTEUR), 
						getQuantite(key, AILE));
				setUsine(usine, noeuds, listeObserver);
				break;
			case Constante.NOM_ENTREPOT:
				key = NOM_ENTREPOT;
				usine = new Entrepot(Integer.parseInt(uSim.getId()), 
						new Point(Integer.parseInt(uSim.getX()), Integer.parseInt(uSim.getY())), 
						vu.get(key), new Avion(), getLimite(key, AVION));
				setUsine(usine, noeuds, listeObserver);
				sujet = (Entrepot) usine;
				ChoixStrategie.addEntrepot((Entrepot)usine);
				break;
			default:
				System.out.println(Constante.ERROR0002);
				break;
			}
		}
		setSujet(noeuds, sujet);
	}

	private void setUsine(Usine u, List<Noeud> noeuds, HashSet<Observer> listeObserver) {
		noeuds.add(u);
		if(u instanceof UsineProductrice) {
			UsineProductrice up = (UsineProductrice) u;
			listeObserver.add(up);
		}
		else if(u instanceof Entrepot) {
			Entrepot e = (Entrepot) u;
			e.setListeObserver(listeObserver);
		}
	}
	
	private void setSujet(List<Noeud> noeuds, Subject sujet) {
		for (Noeud noeud : noeuds) {
			if(noeud instanceof UsineProductrice) {
				((UsineProductrice) noeud).setSujet(sujet);
			}
		}
	}
	
	private void creerChemin(List<Noeud> noeuds) {
		for(XMLCheminSimulation uChem : config.getSimulation().getChemins().getChemin()) {
			UsineProductrice uDep = null;
				for(Noeud noeud : noeuds) {
					if(noeud.getId() == Integer.parseInt(uChem.getDe())) {
						uDep = (UsineProductrice) noeud;
					}
				}
			Usine uFin = null;
			for(Noeud noeud : noeuds) {
				if(noeud.getId() == Integer.parseInt(uChem.getVers())) {
					uFin = (Usine) noeud;
				}
			}
			noeuds.add(new CheminReseau(uDep, uFin, PointE.vecteurUnitaire((uFin.getX() - uDep.getX()), (uFin.getY() - uDep.getY())) ));
		}
	}
}
