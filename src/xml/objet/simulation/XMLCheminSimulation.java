package xml.objet.simulation;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/*
CODE EMPRUNT� :
Les lignes suivantes sont bas�es sur un exemple provenant du site :
   https://www.baeldung.com/xstream-deserialize-xml-to-object
*/	
@XStreamAlias("chemin")
public class XMLCheminSimulation {


	@XStreamAsAttribute
	private String de;
	
	@XStreamAsAttribute
	private String vers;
/* FIN DU CODE EMPRUNT� */

	public String getDe() {
		return de;
	}

	public void setDe(String de) {
		this.de = de;
	}

	public String getVers() {
		return vers;
	}

	public void setVers(String vers) {
		this.vers = vers;
	}
}
