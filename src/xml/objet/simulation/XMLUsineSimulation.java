package xml.objet.simulation;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class XMLUsineSimulation {
	/*
	CODE EMPRUNT� :
	Les lignes suivantes sont bas�es sur un exemple provenant du site :
	   https://www.baeldung.com/xstream-deserialize-xml-to-object
	*/	
		@XStreamAsAttribute
		private String type;
		
		@XStreamAsAttribute
		private String id;
		
		@XStreamAsAttribute
		private String x;
		
		@XStreamAsAttribute
		private String y;
	/* FIN DU CODE EMPRUNT� */
		
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getX() {
		return x;
	}
	public void setX(String x) {
		this.x = x;
	}

	public String getY() {
		return y;
	}
	public void setY(String y) {
		this.y = y;
	}
	
	
}
