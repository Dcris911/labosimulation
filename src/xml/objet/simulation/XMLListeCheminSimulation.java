package xml.objet.simulation;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/*
CODE EMPRUNT� :
Les lignes suivantes sont bas�es sur un exemple provenant du site :
   https://www.baeldung.com/xstream-deserialize-xml-to-object
*/	
@XStreamAlias("chemins")
public class XMLListeCheminSimulation {

	
	@XStreamImplicit
	private List<XMLCheminSimulation> chemin;
/* FIN DU CODE EMPRUNT� */

	public List<XMLCheminSimulation> getChemin() {
		return chemin;
	}

	public void setChemin(List<XMLCheminSimulation> chemin) {
		this.chemin = chemin;
	}
	
}
