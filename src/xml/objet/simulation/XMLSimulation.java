package xml.objet.simulation;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/*
CODE EMPRUNT� :
Les lignes suivantes sont bas�es sur un exemple provenant du site :
   https://www.baeldung.com/xstream-deserialize-xml-to-object
*/	
@XStreamAlias("simulation")
public class XMLSimulation {

	@XStreamImplicit
	private List<XMLUsineSimulation> usineSim;
	
	@XStreamImplicit
	private XMLListeCheminSimulation chemins;
/* FIN DU CODE EMPRUNT� */
	
	public List<XMLUsineSimulation> getUsineSim() {
		return usineSim;
	}
	public void setUsineSim(List<XMLUsineSimulation> usineSim) {
		this.usineSim = usineSim;
	}
	
	public XMLListeCheminSimulation getChemins() {
		return chemins;
	}
	public void setChemins(XMLListeCheminSimulation chemins) {
		this.chemins = chemins;
	}
		
}
