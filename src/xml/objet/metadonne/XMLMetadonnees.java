package xml.objet.metadonne;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class XMLMetadonnees {

	/*
	   CODE EMPRUNT� :
	   Les lignes suivantes sont bas�es sur un exemple provenant du site :
	      https://www.baeldung.com/xstream-deserialize-xml-to-object
	*/
		@XStreamImplicit
		private List<XMLUsineMeta> usine;
	/* FIN DU CODE EMPRUNT� */
		
	public List<XMLUsineMeta> getUsine() {
		return usine;
	}

	public void setUsine(List<XMLUsineMeta> usine) {
		this.usine = usine;
	}
	
	
}
