package xml.objet.metadonne;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class XMLListeIcones {

	/*
	   CODE EMPRUNT� :
	   Les lignes suivantes sont bas�es sur un exemple provenant du site :
	      https://www.baeldung.com/xstream-deserialize-xml-to-object
	*/
		@XStreamImplicit
		private List<XMLIcone> icone;
	/* FIN DU CODE EMPRUNT� */
	
	public List<XMLIcone> getIcone() {
		return icone;
	}

	public void setIcone(List<XMLIcone> icone) {
		this.icone = icone;
	}
	
}
