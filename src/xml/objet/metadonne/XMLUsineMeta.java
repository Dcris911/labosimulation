package xml.objet.metadonne;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class XMLUsineMeta {

	/*
	   CODE EMPRUNT� :
	   Les lignes suivantes sont bas�es sur un exemple provenant du site :
	      https://www.baeldung.com/xstream-deserialize-xml-to-object
	*/
		@XStreamAsAttribute
		private String type;
	
		@XStreamImplicit
		private XMLListeIcones icones;
	
		@XStreamImplicit
		private XMLSortie sortie;
	
		@XStreamImplicit
		private List<XMLEntree> entree;
		
		@XStreamAlias("interval-production")
		private int intervalProduction;
	/* FIN DU CODE EMPRUNT� */
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public XMLListeIcones getIcones() {
		return icones;
	}

	public void setIcones(XMLListeIcones icones) {
		this.icones = icones;
	}

	public XMLSortie getSortie() {
		return sortie;
	}

	public void setSortie(XMLSortie sortie) {
		this.sortie = sortie;
	}

	public int getIntervalProduction() {
		return intervalProduction;
	}

	public void setIntervalProduction(int intervalProduction) {
		this.intervalProduction = intervalProduction;
	}

	public List<XMLEntree> getEntree() {
		return entree;
	}

	public void setEntree(List<XMLEntree> entree) {
		this.entree = entree;
	}
}
