package xml.objet.metadonne;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class XMLSortie {

	/*
	   CODE EMPRUNT� :
	   Les lignes suivantes sont bas�es sur un exemple provenant du site :
	      https://www.baeldung.com/xstream-deserialize-xml-to-object
	*/
		@XStreamAsAttribute
		private String type;
	/* FIN DU CODE EMPRUNT� */

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
