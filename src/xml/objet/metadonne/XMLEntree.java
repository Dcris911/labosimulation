package xml.objet.metadonne;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class XMLEntree {

	/*
	   CODE EMPRUNT� :
	   Les lignes suivantes sont bas�es sur un exemple provenant du site :
	      https://www.baeldung.com/xstream-deserialize-xml-to-object
	*/	
		@XStreamAsAttribute
		private String type;
	
		@XStreamAsAttribute
		private String quantite;
	
		@XStreamAsAttribute
		private String capacite;
	/* FIN DU CODE EMPRUNT� */
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getQuantite() {
		return quantite;
	}

	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}

	public String getCapacite() {
		return capacite;
	}

	public void setCapacite(String capacite) {
		this.capacite = capacite;
	}
}
