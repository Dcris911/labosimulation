package xml.objet.metadonne;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class XMLIcone {

	/*
	   CODE EMPRUNT� :
	   Les lignes suivantes sont bas�es sur un exemple provenant du site :
	      https://www.baeldung.com/xstream-deserialize-xml-to-object
	*/
		@XStreamAsAttribute
		private String type;
		
		@XStreamAsAttribute
		private String path;
	/* FIN DU CODE EMPRUNT� */

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
