package xml.objet;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import xml.objet.metadonne.XMLMetadonnees;
import xml.objet.simulation.XMLSimulation;

public class XMLConfiguration {

	/*
	   CODE EMPRUNT� :
	   Les lignes suivantes sont bas�es sur un exemple provenant du site :
	      https://www.baeldung.com/xstream-deserialize-xml-to-object
	*/	
		@XStreamImplicit
		private XMLMetadonnees metadonnees;
		
		@XStreamImplicit
		private XMLSimulation simulation;
	/* FIN DU CODE EMPRUNT� */
		
	public XMLSimulation getSimulation() {
		return simulation;
	}

	public void setSimulation(XMLSimulation simulation) {
		this.simulation = simulation;
	}

	public XMLMetadonnees getMetadonnees() {
		return metadonnees;
	}

	public void setMetadonnees(XMLMetadonnees metadonnees) {
		this.metadonnees = metadonnees;
	}

}
