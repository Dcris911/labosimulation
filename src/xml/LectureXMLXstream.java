package xml;


import java.io.File;
import java.io.FileInputStream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import xml.objet.XMLConfiguration;
import xml.objet.metadonne.XMLEntree;
import xml.objet.metadonne.XMLIcone;
import xml.objet.metadonne.XMLListeIcones;
import xml.objet.metadonne.XMLMetadonnees;
import xml.objet.metadonne.XMLSortie;
import xml.objet.metadonne.XMLUsineMeta;
import xml.objet.simulation.XMLCheminSimulation;
import xml.objet.simulation.XMLListeCheminSimulation;
import xml.objet.simulation.XMLSimulation;
import xml.objet.simulation.XMLUsineSimulation;

/**
 * La classe est bas� sur les exemples des sites
 * https://x-stream.github.io/tutorial.html 
 * et 
 * https://www.baeldung.com/xstream-deserialize-xml-to-object
 * 
 * J'ai pris la librairie XStream pour lire des fichiers XML.
 */
public class LectureXMLXstream {

	private String path;
	private XStream stream;
	
	public LectureXMLXstream(String path) {
		this.path = path;
		this.stream = null;
	}
	
	public XMLConfiguration lectureXML() {
		return openFile();
	}
	        
	private XMLConfiguration openFile() {
		XMLConfiguration configuration = null;
		try {
			System.out.println(path);
			if(new File(path).exists()) {
				/*
				   CODE EMPRUNT� :
				   Les lignes suivantes sont bas�es sur un exemple provenant du site :
				      https://x-stream.github.io/tutorial.html
				*/	
					stream = new XStream(new DomDriver());
					alias();
					configuration = (XMLConfiguration) stream.fromXML(new FileInputStream(path));
				/* FIN DU CODE EMPRUNT� */
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Exception");
		}
		return configuration;
	}
	
	/*
	   CODE EMPRUNT� :
	   Les lignes suivantes sont bas�es sur un exemple provenant du site :
	      https://www.baeldung.com/xstream-deserialize-xml-to-object
	*/	
		private void alias() {
			stream.alias("configuration", XMLConfiguration.class);
			aliasMetadonnee();
			aliasSimulation();
		}
		
		private void aliasMetadonnee() {
			stream.alias("metadonnees", XMLMetadonnees.class);
			stream.alias("usine", XMLUsineMeta.class);
			stream.alias("icones", XMLListeIcones.class);
			stream.alias("icone", XMLIcone.class);
			stream.alias("sortie", XMLSortie.class);
			stream.alias("entree", XMLEntree.class);
			
			stream.aliasField("interval-production", XMLUsineMeta.class, "intervalProduction");
			
			stream.useAttributeFor(XMLUsineMeta.class, "type");
			stream.useAttributeFor(XMLIcone.class, "type");
			stream.useAttributeFor(XMLIcone.class, "path");
			stream.useAttributeFor(XMLSortie.class, "type");
			stream.useAttributeFor(XMLEntree.class, "type");
			stream.useAttributeFor(XMLEntree.class, "quantite");
			stream.useAttributeFor(XMLEntree.class, "capacite");
			
			stream.addImplicitCollection(XMLMetadonnees.class, "usine");
			stream.addImplicitCollection(XMLListeIcones.class, "icone");
			stream.addImplicitCollection(XMLUsineMeta.class, "entree");
			
		}
		
		private void aliasSimulation() {
			stream.alias("simulation", XMLSimulation.class);
			stream.alias("usineSim", XMLUsineSimulation.class);
			stream.alias("chemins", XMLListeCheminSimulation.class);
			stream.alias("chemin", XMLCheminSimulation.class);
			
			stream.useAttributeFor(XMLCheminSimulation.class, "de");
			stream.useAttributeFor(XMLCheminSimulation.class, "vers");
			stream.useAttributeFor(XMLUsineSimulation.class, "type");
			stream.useAttributeFor(XMLUsineSimulation.class, "id");
			stream.useAttributeFor(XMLUsineSimulation.class, "x");
			stream.useAttributeFor(XMLUsineSimulation.class, "y");
			
			stream.addImplicitCollection(XMLSimulation.class, "usineSim");
			stream.addImplicitCollection(XMLListeCheminSimulation.class, "chemin");
		}
	/* FIN DU CODE EMPRUNT� */
}
