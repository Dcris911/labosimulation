package com.strategie;

public interface StrategieVente {

	/**
	 * M�thode servant � v�rifier si un avion doit �tre vendu
	 * @return true si un avion peut-�tre vendu
	 */
	public boolean venteAvion();
}
