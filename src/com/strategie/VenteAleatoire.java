package com.strategie;

import java.util.Random;

import com.util.Constante;

public class VenteAleatoire implements StrategieVente {

	private static Random random = new Random();
	
	public VenteAleatoire() {
		super();
		System.out.println("Strat�gie de vente : Vente Al�atoire");
	}

	@Override
	public boolean venteAvion() {
		return (random.nextInt(Constante.SEED_MAXIMAL_RANDOM) == 0);
	}

}
