package com.strategie;

import com.util.Constante;

public class VenteFixe implements StrategieVente {
	
	private int compteur;
	
	public VenteFixe() {
		super();
		System.out.println("Stratégie de vente : Vente Fixe");
		compteur = 0;
	}

	@Override
	public boolean venteAvion() {
		if(compteur >= Constante.NB_TOUR_ATTENTE) {
			compteur = 0;
			return true;
		}
		compteur++;
		return false;
	}

}
