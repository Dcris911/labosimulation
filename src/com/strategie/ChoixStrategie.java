package com.strategie;

import java.util.ArrayList;
import java.util.List;

import com.usine.Entrepot;
import com.util.Constante;

public class ChoixStrategie {

	private static List<Entrepot> listEntrepot = new ArrayList<>();
	
	public static void addEntrepot(Entrepot entrepot) {
		listEntrepot.add(entrepot);
	}
	
	public static void changerStragtegy(String strategieVoulu) {
		StrategieVente strategieVente;
		switch (strategieVoulu) {
			case Constante.STRATEGY_ALEATOIRE:
				strategieVente = new VenteAleatoire();
				break;
			case Constante.STRATEGY_FIXE:
				strategieVente = new VenteFixe();
				break;
			case Constante.STRATEGY_DEFAUT:
			default:
				strategieVente = new VenteFixe();
				break;
		}
		if(listEntrepot.size() > 0) {
			for (Entrepot entrepot : listEntrepot) {
				entrepot.setStrategieVente(strategieVente);
			}
		}else {
			System.out.println(Constante.ERROR0004);
		}
	}
}
