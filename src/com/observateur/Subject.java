package com.observateur;

public interface Subject {

	/**
	 * Ajoute un observateur � la liste du sujet
	 * @param o L'observateur
	 * @return Si l'observateur a pu �tre ajouter
	 */
	public boolean 	addObserver(Observer o);
	
	/**
	 * Retire un observateur de la liste du sujet
	 * @param o L'observateur
	 * @return Si l'observateur a bien �t�t retir�
	 */
	public boolean 	removeObserver(Observer o);
	
	/**
	 * Notifie tous les observateurs
	 */
	public void 	notifyAllObservers();
	
}
