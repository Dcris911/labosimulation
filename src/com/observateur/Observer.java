package com.observateur;

public interface Observer {

	/**
	 * Mettre � jour l'observateur
	 */
	public void update();
}
