package com.composante;

import java.awt.Image;

import com.util.Constante;
import com.util.PointE;

public class Moteur extends Composante {

	private static Image icon;

	public Moteur() {
		super(null);
	}
	public Moteur(PointE position) {
		super(position);
	}

	@Override
	public Image getImage() {
		return icon;
	}

	@Override
	public void setImageComposante(Image icon) {
		Moteur.icon = icon;
	}

	@Override
	public boolean isSameInstance(Composante c) {
		return (c instanceof Moteur);
	}

	@Override
	public String getNom() {
		return Constante.MOTEUR;
	}

}
