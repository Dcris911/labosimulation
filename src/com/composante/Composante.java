package com.composante;

import java.awt.Image;

import com.util.PointE;

/**
 * @author David Gilbert
 * Classe d�finissant les diff�rents attributs communs � chacun des composantes de la simulation
 */
public abstract class Composante {

	private static int nodeId = 1000;
	
	protected 	int 	id;
	protected 	PointE 	position;
		
	public Composante(PointE position) {
		this(position, nodeId++);
	}
	public Composante(PointE position, int id) {
		super();
		this.id = id;
		this.position = position;
	}
	
	public int getX() {
		return position.getXi();
	}
	public int getY() {
		return position.getYi();
	}

	public int getId() {
		return id;
	}
	
	@Override
	public boolean equals(Object objet) {
		if( objet == null || (!(objet instanceof Composante))) {
			return false;
		}
		if(this == objet) { 
			return true; 
		}
		Composante obj = (Composante)objet;
		return isSameInstance(obj) && (id == obj.id);
	}

	public void bouger(PointE vecteur) {
		position.translate(vecteur.getXf(), vecteur.getYf());
	}
	
	/**
	 * Recherche l'image du composante
	 * @return l'image du composante
	 */
	public abstract Image getImage();
	
	/**
	 * Set l'image du composante
	 * @param image L'image
	 */
	public abstract void setImageComposante(Image image);
	
	/**
	 * V�rifie si la composante est du m�me type que la composante pass� en param�tre
	 * @param c La composante � v�rifier
	 * @return Si la composante est du m�me type
	 */
	public abstract boolean isSameInstance(Composante c);
	
	/**
	 * @return Le nom de la composante
	 */
	public abstract String getNom();
}
