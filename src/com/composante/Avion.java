package com.composante;

import java.awt.Image;

import com.util.Constante;
import com.util.PointE;

public class Avion extends Composante {

	private static Image icon;
	
	public Avion() {
		super(null);
	}
	public Avion(PointE position) {
		super(position);
	}

	@Override
	public Image getImage() {
		return icon;
	}

	@Override
	public void setImageComposante(Image icon) {
		Avion.icon = icon;
	}

	@Override
	public boolean isSameInstance(Composante c) {
		return (c instanceof Avion);
	}

	@Override
	public String getNom() {
		return Constante.AVION;
	}

	@Override
	public String toString() {
		return Constante.VENTE_AVION + id;
	}
}
