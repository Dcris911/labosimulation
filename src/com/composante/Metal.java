package com.composante;

import java.awt.Image;

import com.util.Constante;
import com.util.PointE;

public class Metal extends Composante{

	private static Image icon;

	public Metal() {
		super(null);
	}
	public Metal(PointE position) {
		super(position);
	}

	@Override
	public Image getImage() {
		return icon;
	}

	@Override
	public void setImageComposante(Image icon) {
		Metal.icon = icon;
	}

	@Override
	public boolean isSameInstance(Composante c) {
		return (c instanceof Metal);
	}

	@Override
	public String getNom() {
		return Constante.METAL;
	}

}
