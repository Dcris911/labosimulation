package com.composante;

import java.awt.Image;

import com.util.Constante;
import com.util.PointE;

public class Aile extends Composante {

	private static Image icon;
	
	public Aile() {
		super(null);
	}
	public Aile(PointE position) {
		super(position);
	}

	@Override
	public Image getImage() {
		return icon;
	}
	
	@Override
	public void setImageComposante(Image icon) {
		Aile.icon = icon;
	}
	
	@Override
	public boolean isSameInstance(Composante c) {
		return (c instanceof Aile);
	}
	
	@Override
	public String getNom() {
		return Constante.AILE;
	}
}
