package com;

import static com.util.Constante.PATH_AILE;
import static com.util.Constante.PATH_AVION;
import static com.util.Constante.PATH_METAL;
import static com.util.Constante.PATH_MOTEUR;

import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.composante.Aile;
import com.composante.Avion;
import com.composante.Metal;
import com.composante.Moteur;
import com.strategie.ChoixStrategie;
import com.util.Constante;

import xml.ServiceXML;

/**
 * @author David Gilbert
 * Classe contenant tous les noeuds qui seront utilis�s par l'application.
 */
public class Entreprise {

	private List<Noeud> noeuds;
	private ChoixStrategie cs;
	
	/**
	 * Constructeur
	 */
	public Entreprise() {
		super();
		noeuds = new ArrayList<>();
		cs = new ChoixStrategie();
		initialisationXML(Constante.EMPTY_STRING);
	}

	/**
	 * M�thode servant � initialiser une classe Entreprise depuis un fichier XML
	 * @param path Le chemin vers le fichier
	 */
	public void creerEntrepriseFromXMLFile(String path) {
		initialisationXML(path);
	}
	
	private void initialisationXML(String path) {
		noeuds = new ArrayList<>();
		if(path.trim().isEmpty()) {
			path = Constante.PATH_CONFIGURATION_RELATIVE;
		}
		ServiceXML serviceXML = new ServiceXML(path);		
		noeuds = serviceXML.creerListeNoeud(cs);
		
		configurationImageComposante();
		
		ChoixStrategie.changerStragtegy(Constante.STRATEGY_DEFAUT);
	}
	
	private void configurationImageComposante() {
		try {
			new Metal().setImageComposante(ImageIO.read(new File(PATH_METAL)));
			new Avion().setImageComposante(ImageIO.read(new File(PATH_AVION)));
			new Moteur().setImageComposante(ImageIO.read(new File(PATH_MOTEUR)));
			new Aile().setImageComposante(ImageIO.read(new File(PATH_AILE)));
		}catch (IOException e) {
			System.out.println(Constante.ERROR0001A);
		}
	}

	/**
	 * Permet de dessiner tous les noeuds contenus dans l'entreprise
	 * @param g Le graphics ou sera imprim� les diff�rents noeuds
	 */
	public void paint(Graphics g) {
		for (Noeud noeud : noeuds) {
			noeud.drawNoeud(g);
		}
	}
	
	/**
	 * Permet de mettre � jour (l'�quivalent d'un tour) tous les noeuds de l'entreprise
	 */
	public void updateNoeuds() {
		for (Noeud noeud : noeuds) {
			noeud.updateNoeud();
		}
	}
}
