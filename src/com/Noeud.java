package com;

import java.awt.Graphics;

public abstract class Noeud {

	private static int nodeId = 100;
	protected int id;
	
	public Noeud() {
		super();
		id = nodeId++;
	}
	
	public Noeud(int id) {
		super();
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	/**
	 * Mets � jour le noeud
	 */
	public abstract void updateNoeud();
	
	/**
	 * Dessine le noeud dans le graphic
	 * @param g Le graphic
	 */
	public abstract void drawNoeud(Graphics g);
}
