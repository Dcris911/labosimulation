package com.reseau;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import com.Noeud;
import com.composante.Composante;
import com.usine.Entrepot;
import com.usine.Usine;
import com.usine.UsineProductrice;
import com.util.Constante;
import com.util.PointE;

/**
 * Classe permettant de d�placer des composantes d'une usine � l'autre
 * @author David Gilbert
 */
public class CheminReseau extends Noeud {

	private UsineProductrice 	usineDepart;
	private Usine 				usineArrive;
	private List<Composante> 	composanteEnChemin;
	private PointE 				vecteurDeplacement;
	
	
	public CheminReseau(UsineProductrice usineDepart, Usine usineArrive, PointE vecteurDeplacement) {
		super();
		this.usineDepart 		= usineDepart;
		this.usineArrive 		= usineArrive;
		this.vecteurDeplacement = vecteurDeplacement;
		this.composanteEnChemin = new ArrayList<>();
	}

	@Override
	public void drawNoeud(Graphics g) {
		g.drawLine(getUsineDepart().getX(), getUsineDepart().getY(), getUsineArrive().getX(), getUsineArrive().getY());
		for (Composante composante : composanteEnChemin) {
			g.drawImage(composante.getImage(), ajusterPositionSelonImage(composante.getX()), ajusterPositionSelonImage(composante.getY()), 
					Constante.TAILLE_COMPOSANTE, Constante.TAILLE_COMPOSANTE, null);
		}
	}
	
	private int ajusterPositionSelonImage(int position) {
		return (position - (Constante.TAILLE_COMPOSANTE/2));
	}
	
	@Override
	public void updateNoeud() {
		//Boucler et faire bouger tous les Composante
		List<Composante> toRemove = new ArrayList<>();
		for (Composante composante : composanteEnChemin) {
			bougerComposante(composante, toRemove);
		}
		composanteEnChemin.removeAll(toRemove);
		// Mettre des composantes sur le chemin
		if(usineDepart.isComposantePretEnvoie() && limiteCheminNonDepasser() && entrepotNonPlein()) {
			composanteEnChemin.add(usineDepart.envoyerDerniereComposante());
		}
	}

	/**
	 * D�place les composantes sur le chemin et v�rifie si la composante doit d�barquer du r�seau
	 * @param composante La composante � bouger
	 * @param toRemove Les composantes qui seront peut-�tre � retirer � la fin des d�placements
	 */
	private void bougerComposante(Composante composante, List<Composante> toRemove) {
		for(int i = 0; i<Constante.NB_DEPLACEMENT_PAR_TOUR; i++) {
			composante.bouger(vecteurDeplacement);
			//verifier si la composante est arriver � l'usine
			if(composanteRenduUsineArrive(composante)) {
				if(usineArrive.recevoirComposante(composante)) {
					toRemove.add(composante);
				}
				else {
					System.out.println(Constante.ERROR0003);
				}
				return;
			}
		}
	}

	private boolean limiteCheminNonDepasser() {
		return (composanteEnChemin.size() < Constante.LIMITE_COMPOSANTE_PAR_CHEMIN);
	}

	private boolean entrepotNonPlein() {
		if(usineArrive instanceof Entrepot) {
			Entrepot e = (Entrepot) usineArrive;
			return (e.getSizeEntrepose() + composanteEnChemin.size() <= e.getLimite()); 
		}
		return true;
	}
	
	private boolean composanteRenduUsineArrive(Composante c) {
		return (c.getX()) == usineArrive.getX()	&& c.getY() == usineArrive.getY();
	}

	public void ajouterComposante(Composante c) {
		composanteEnChemin.add(c);
	}
	public boolean retirerComposante(Composante c) {
		return composanteEnChemin.remove(c);
	}

	public UsineProductrice getUsineDepart() {
		return usineDepart;
	}
	public Usine getUsineArrive() {
		return usineArrive;
	}

	public PointE getVecteurDeplacement() {
		return vecteurDeplacement;
	}
	
}
