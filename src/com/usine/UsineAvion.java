package com.usine;

import java.awt.Point;

import com.composante.Aile;
import com.composante.Avion;
import com.composante.Composante;
import com.composante.Moteur;
import com.util.Constante;
import com.util.PointE;
import com.util.VisuelUsine;

public class UsineAvion extends UsineProductrice {

	private int quantiteMoteur;
	private int quantiteAiles;
	
	public UsineAvion(Point coordonneUsine, VisuelUsine visuelUsine, int tempsFabrication, int nbMoteurRequis, int nbAileRequis){
		super(coordonneUsine, visuelUsine, new Avion(), tempsFabrication);
		quantiteAiles  = 0;
		quantiteMoteur = 0;
		composanteRequisCreation.addComposanteRequis(new Aile(), nbAileRequis);
		composanteRequisCreation.addComposanteRequis(new Moteur(), nbMoteurRequis);
	}
	
	public UsineAvion(int id, Point coordonneUsine, VisuelUsine visuelUsine, int tempsFabrication, int nbMoteurRequis, int nbAileRequis){
		super(id, coordonneUsine, visuelUsine, new Avion(), tempsFabrication);
		quantiteAiles  = 0;
		quantiteMoteur = 0;
		composanteRequisCreation.addComposanteRequis(new Aile(), nbAileRequis);
		composanteRequisCreation.addComposanteRequis(new Moteur(), nbMoteurRequis);
	}

	@Override
	public void updateNoeud() {
		if(creeValide()) {
			if(timerFabrication >= tempsFabrication) {
				creer();
			}else {
				timerFabrication+=Constante.INCREMENTEUR;
			}
		}
		updateNoeudCommunUsineProduction();
	}

	private boolean creeValide() {
		return !isEnArret() 
				&& quantiteMoteur >= composanteRequisCreation.getNombreRequis(new Moteur())
				&& quantiteAiles >= composanteRequisCreation.getNombreRequis(new Aile());
	}
	
	@Override
	public void creer() {
		quantiteComposanteCree.add(new Avion(new PointE(coordonneUsine)));
		timerFabrication = 0;
		quantiteMoteur-= composanteRequisCreation.getNombreRequis(new Moteur());
		quantiteAiles-= composanteRequisCreation.getNombreRequis(new Aile());
	}

	@Override
	public boolean recevoirComposante(Composante c) {
		if(c instanceof Moteur) {
			quantiteMoteur++;
			return true;
		}
		else if(c instanceof Aile) {
			quantiteAiles++;
			return true;
		}
		return false;
	}

	@Override
	public String nomUsine() {
		return Constante.NOM_USINE_AVION;
	}

}
