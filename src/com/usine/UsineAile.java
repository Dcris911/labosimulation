package com.usine;

import java.awt.Point;

import com.composante.Aile;
import com.composante.Composante;
import com.composante.Metal;
import com.util.Constante;
import com.util.PointE;
import com.util.VisuelUsine;

public class UsineAile extends UsineProductrice {

	private int quantiteMetal;
	
	public UsineAile(Point coordonneUsine, VisuelUsine visuelUsine,	int tempsFabrication, int nbMetalRequis) {
		super(coordonneUsine, visuelUsine, new Aile(), tempsFabrication);
		quantiteMetal = 0;
		composanteRequisCreation.addComposanteRequis(new Metal(), nbMetalRequis);
	}
	public UsineAile(int id, Point coordonneUsine, VisuelUsine visuelUsine,	int tempsFabrication, int nbMetalRequis) {
		super(id, coordonneUsine, visuelUsine, new Aile(), tempsFabrication);
		quantiteMetal = 0;
		composanteRequisCreation.addComposanteRequis(new Metal(), nbMetalRequis);
	}

	@Override
	public void updateNoeud() {
		if(!isEnArret() && quantiteMetal >= composanteRequisCreation.getNombreRequis(new Metal())) {
			if(timerFabrication >= tempsFabrication) {
				creer();
			}else {
				timerFabrication+=Constante.INCREMENTEUR;
			}
		}
		updateNoeudCommunUsineProduction();
	}
	
	@Override
	public void creer() {
		quantiteComposanteCree.add(new Aile(new PointE(coordonneUsine)));
		timerFabrication = 0;
		quantiteMetal-= composanteRequisCreation.getNombreRequis(new Metal());
	}

	@Override
	public boolean recevoirComposante(Composante c) {
		if(c instanceof Metal) {
			quantiteMetal++;
			return true;
		}
		return false;
	}

	@Override
	public String nomUsine() {
		return Constante.NOM_USINE_AILE;
	}

}
