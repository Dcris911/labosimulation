package com.usine;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.composante.Composante;
import com.observateur.Observer;
import com.observateur.Subject;
import com.strategie.StrategieVente;
import com.util.Constante;
import com.util.VisuelUsine;

public class Entrepot extends Usine implements Subject{

	private Composante 			typeEntrepot;
	private int 				limite;
	private List<Composante> 	composanteEntrepose;
	private StrategieVente 		strategieVente;
	private HashSet<Observer> 	listeObserver;
		
	public Entrepot(Point coordonneUsine, VisuelUsine visuelUsine, Composante typeEntrepot, int limite) {
		super(coordonneUsine, visuelUsine);
		this.typeEntrepot 			= typeEntrepot;
		this.limite 				= limite;
		this.strategieVente 		= null;
		this.composanteEntrepose 	= new ArrayList<>();
		this.listeObserver 			= new HashSet<Observer>();
	}
	
	public Entrepot(int id, Point coordonneUsine, VisuelUsine visuelUsine, Composante typeEntrepot, int limite) {
		super(id, coordonneUsine, visuelUsine);
		this.typeEntrepot 			= typeEntrepot;
		this.limite 				= limite;
		this.strategieVente 		= null;
		this.composanteEntrepose 	= new ArrayList<>();
		this.listeObserver 			= new HashSet<Observer>();
	}

	@Override
	public void updateNoeud() {
		if(isComposantePretEnvoie()) {
			if(strategieVente.venteAvion()) {
				System.out.println(envoyerDerniereComposante());
			}
		}
		changerEtatVisuelUsine(etatVisuelEntrepot());
	}

	@Override
	public boolean recevoirComposante(Composante c) {
		if(c.isSameInstance(typeEntrepot)) {
			composanteEntrepose.add(c);
			notifyAllObservers();
			return true;
		}
		return false;
	}

	@Override
	public boolean retraitComposante(Composante c) {
		boolean flag = false;
		if(typeEntrepot.isSameInstance(c)) {
			flag = composanteEntrepose.remove(c);
			notifyAllObservers();
		}
		return flag;
	}

	@Override
	public Composante envoyerDerniereComposante() {
		Composante c = null;
		int sizeList = composanteEntrepose.size();
		
		if(sizeList > 0) {
			c = composanteEntrepose.get(sizeList -1);
			composanteEntrepose.remove(sizeList -1);
			notifyAllObservers();
		}
		
		return c;
	}
	
	/**
	 * @return L'�tat visuel sous forme d'entier
	 */
	public int etatVisuelEntrepot() {
		if(limite == 0) {
			return Constante.ETAT_PLEIN;
		}
		return (int)((composanteEntrepose.size()*Constante.ETAT_PLEIN)/limite);
	}
	
	public boolean isEntrepotComplet() {
		return (composanteEntrepose.size()>=limite);
	}
	
	@Override
	public boolean isComposantePretEnvoie() {
		return !composanteEntrepose.isEmpty();
	}
	
	public Composante getTypeEntrepot() {
		return typeEntrepot;
	}
	public void setTypeEntrepot(Composante typeEntrepot) {
		this.typeEntrepot = typeEntrepot;
	}

	public int getLimite() {
		return limite;
	}
	public void setLimite(int limite) {
		this.limite = limite;
	}

	public StrategieVente getStrategieVente() {
		return strategieVente;
	}
	public void setStrategieVente(StrategieVente strategieVente) {
		this.strategieVente = strategieVente;
	}

	public List<Composante> getComposanteEntrepose() {
		return composanteEntrepose;
	}
	public int getSizeEntrepose() {
		return composanteEntrepose.size();
	}
	public HashSet<Observer> getListeObserver() {
		return listeObserver;
	}
	public void setListeObserver(HashSet<Observer> l) {
		this.listeObserver = l;
	}

	@Override
	public boolean addObserver(Observer o) {
		return listeObserver.add(o);
	}
	@Override
	public boolean removeObserver(Observer o) {
		return listeObserver.remove(o);
	}
	@Override
	public void notifyAllObservers() {
		for (Observer observer : listeObserver) {
			observer.update();
		}
	}

	@Override
	public String nomUsine() {
		return Constante.NOM_ENTREPOT;
	}

}
