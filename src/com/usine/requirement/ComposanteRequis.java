package com.usine.requirement;

import java.util.ArrayList;
import java.util.List;

import com.composante.Composante;

public class ComposanteRequis {

	private List<ComposanteEtNombre> listeComposante;

	public ComposanteRequis() {
		super();
		this.listeComposante = new ArrayList<>();
	}

	/**
	 * Ajoute une composante et le nombre requis pour la fabrication
	 * @param c La composante
	 * @param nb Le nombre requis de composante pour la fabrication
	 * @return Si l'ajout c'est bien fait
	 */
	public boolean addComposanteRequis(Composante c, int nb) {
		for (ComposanteEtNombre composanteEtNombre : listeComposante) {
			if(c.isSameInstance(composanteEtNombre.getTypeComposante())) {
				return false;
			}
		}
		return listeComposante.add(new ComposanteEtNombre(c, nb));
	}

	/**
	 * @param c La composante dont on cheche le nombre requis
	 * @return Le nombre de composante requis pour la fabrication
	 */
	public int getNombreRequis(Composante c) {
		for (ComposanteEtNombre composanteEtNombre : listeComposante) {
			if(c.isSameInstance(composanteEtNombre.getTypeComposante())) {
				return composanteEtNombre.getNbRequis();
			}
		}
		return -1;
	}
	
}
