package com.usine.requirement;

import com.composante.Composante;

public class ComposanteEtNombre {

	private Composante 	typeComposante;
	private int 		nbRequis;
	
	public ComposanteEtNombre(Composante typeComposante, int nbRequis) {
		super();
		this.typeComposante = typeComposante;
		this.nbRequis 		= nbRequis;
	}
	
	public Composante getTypeComposante() {
		return typeComposante;
	}
	public int getNbRequis() {
		return nbRequis;
	}
	
}
