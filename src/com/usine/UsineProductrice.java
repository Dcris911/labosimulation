package com.usine;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.composante.Composante;
import com.observateur.Observer;
import com.observateur.Subject;
import com.usine.requirement.ComposanteRequis;
import com.util.Constante;
import com.util.VisuelUsine;

public abstract class UsineProductrice extends Usine implements Observer{

	protected Composante 		typeComposanteCree;
	protected List<Composante> 	quantiteComposanteCree;
	protected ComposanteRequis 	composanteRequisCreation;
	protected int 				tempsFabrication;
	protected int 				timerFabrication;
	protected boolean 			enArret;
	protected Subject 			sujet;
	
	public UsineProductrice(Point coordonneUsine, VisuelUsine visuelUsine, Composante typeComposanteCree, int tempsFabrication) {
		super(coordonneUsine, visuelUsine);
		constructeurUsineProduction(typeComposanteCree, tempsFabrication);
	}
	public UsineProductrice(int id, Point coordonneUsine, VisuelUsine visuelUsine, Composante typeComposanteCree, int tempsFabrication) {
		super(id, coordonneUsine, visuelUsine);
		constructeurUsineProduction(typeComposanteCree, tempsFabrication);
	}
	private void constructeurUsineProduction(Composante typeComposanteCree, int tempsFabrication) {
		this.typeComposanteCree 		= typeComposanteCree;
		this.tempsFabrication 			= tempsFabrication;
		this.timerFabrication 			= 0;
		this.quantiteComposanteCree 	= new ArrayList<>();
		this.enArret 					= false;
		this.composanteRequisCreation 	= new ComposanteRequis();
	}
	
	public void updateNoeudCommunUsineProduction() {
		int etatVisuel = (int) ((timerFabrication*Constante.NOMBRE_ETAT)/tempsFabrication);
	    changerEtatVisuelUsine(etatVisuel);
	}
	
	/**
	 * M�thode appel� lorsque le sujet recois une composante.
	 */
	public void update() {
		boolean isEnArret = true;
		if(sujet instanceof Entrepot) {
			Entrepot e = (Entrepot)sujet;
			ajustementRythmeProduction();
			isEnArret = e.isEntrepotComplet();
		}
		mettreEnArret(isEnArret);
	}
	
	public boolean retraitComposante(Composante c) {
		if(typeComposanteCree.isSameInstance(c)) {
			return quantiteComposanteCree.remove(c);
		}
		return false;
	}
	
	public Composante envoyerDerniereComposante() {
		Composante c = null;
		int sizeList = quantiteComposanteCree.size();
		
		if(sizeList != 0) {
			c = quantiteComposanteCree.get(sizeList -1);
			quantiteComposanteCree.remove(sizeList -1);
		}
		
		return c;
	}
	
	public boolean isComposantePretEnvoie() {
		return (!quantiteComposanteCree.isEmpty() && !isEnArret());
	}
	
	public int quantiteAchever() {
		return quantiteComposanteCree.size();
	}
	
	public Composante getTypeComposanteCree() {
		return typeComposanteCree;
	}
	protected List<Composante> getQuantiteComposanteCree() {
		return quantiteComposanteCree;
	}
	public int getTempsFabrication() {
		return tempsFabrication;
	}
	public void setTempsFabrication(int tempsFabrication) {
		this.tempsFabrication = tempsFabrication;
	}

	public boolean isEnArret() {
		return enArret;
	}
	private void mettreEnArret(boolean enArret) {
		this.enArret = enArret;
	}

	public ComposanteRequis getComposanteRequisCreation() {
		return composanteRequisCreation;
	}

	public void setSujet(Subject entrepot) {
		this.sujet = entrepot;
	}
	
	public void ajustementRythmeProduction() {}
	
	/**
	 * Permet la cr�ation de composante
	 */
	public abstract void creer();
}
