package com.usine;

import java.awt.Point;

import com.composante.Composante;
import com.composante.Metal;
import com.util.Constante;
import com.util.PointE;
import com.util.VisuelUsine;

public class UsineMetal extends UsineProductrice {

	private Integer tempsFabricationOriginal;
	
	public UsineMetal(Point coordonneUsine, VisuelUsine visuelUsine, int tempsFabrication) {
		super(coordonneUsine, visuelUsine, new Metal(), tempsFabrication);
	}
	public UsineMetal(int id, Point coordonneUsine, VisuelUsine visuelUsine, int tempsFabrication) {
		super(id, coordonneUsine, visuelUsine, new Metal(), tempsFabrication);
	}

	@Override
	public void updateNoeud() {
		if(!isEnArret()) {
			if(timerFabrication >= tempsFabrication) {
				creer();
			}else {
				timerFabrication+=Constante.INCREMENTEUR;
			}
		}
		updateNoeudCommunUsineProduction();
	}
	
	@Override
	public void creer() {
		quantiteComposanteCree.add(new Metal(new PointE(coordonneUsine)));
		timerFabrication = 0;
	}

	@Override
	public boolean recevoirComposante(Composante c) {
		return false;
	}

	@Override
	public String nomUsine() {
		return Constante.NOM_USINE_METAL;
	}

	@Override
	public void ajustementRythmeProduction(){
		if(sujet instanceof Entrepot && Constante.ACTIVER_AJUSTEMENT_PRODUCTION) {
			Entrepot e = (Entrepot) sujet;
			int etat = e.etatVisuelEntrepot();
			if(tempsFabricationOriginal == null) {
				tempsFabricationOriginal = tempsFabrication;
			}
			float f = tauxProduction(etat);
			tempsFabrication = (int) (f * tempsFabricationOriginal);
			System.out.println(Constante.AJUSTEMENT_PRODUCTION + tempsFabrication);
		}
	}
	
	private float tauxProduction(int etat) {
		if(etat == Constante.ETAT_VIDE) {
			return 1;
		}
		return ((etat+1)/Constante.TAUX_RALENTISSEMENT_USINE_METAL);
	}
	
}
