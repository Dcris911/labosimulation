package com.usine;

import java.awt.Graphics;
import java.awt.Point;

import com.Noeud;
import com.composante.Composante;
import com.util.Constante;
import com.util.VisuelUsine;

public abstract class Usine extends Noeud {

	protected Point 		coordonneUsine;
	protected VisuelUsine 	visuelUsine;

	public Usine(Point coordonneUsine, VisuelUsine visuelUsine) {
		super();
		this.coordonneUsine = coordonneUsine;
		this.visuelUsine 	= visuelUsine;
	}
	public Usine(int id, Point coordonneUsine, VisuelUsine visuelUsine) {
		super(id);
		this.coordonneUsine = coordonneUsine;
		this.visuelUsine 	= visuelUsine;
	}

	public int getX() {
		return (int)coordonneUsine.getX();
	}
	public int getY() {
		return (int)coordonneUsine.getY();
	}
	
	public VisuelUsine getVisuelUsine() {
		return visuelUsine;
	}
	public void changerEtatVisuelUsine(int statut) {
		visuelUsine.setStatut(statut);
	}
	
	@Override
	public void drawNoeud(Graphics g) {
		g.drawImage(visuelUsine.getImage(), ajusterPositionAvecTailleImage(getX()), ajusterPositionAvecTailleImage(getY()),
				Constante.TAILLE_USINE, Constante.TAILLE_USINE, null);
	}
		
	private int ajusterPositionAvecTailleImage(int position) {
		return (position - (Constante.TAILLE_USINE/2));
	}
	
	@Override
	public abstract void updateNoeud();
	
	/**
	 * Retire une composante de la liste
	 * @param c La composante � retirer
	 * @return Si la composante a bien �t� retirer
	 */
	public abstract boolean retraitComposante(Composante  c);
	
	/**
	 * Envoie la derni�re composante qui a �t� cr��
	 * @return La derni�re composante cr��e
	 */
	public abstract Composante envoyerDerniereComposante();
	
	/**
	 * Permet la r�ception d'une composante
	 * @param c La composante � recevoir
	 * @return Si la composante peut-�tre ajout� � la liste
	 */
	public abstract boolean recevoirComposante(Composante c);
	
	/**
	 * @return Si la liste de composante n'est pas vide
	 */
	public abstract boolean isComposantePretEnvoie();
	
	/**
	 * @return le nom de l'usine
	 */
	public abstract String nomUsine();


}
