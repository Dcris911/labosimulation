package com.util;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Classe contenant les images pour les usines
 * @author David Gilbert
 */
public class VisuelUsine {

	private int statut;

	private Image usineVide;
	private Image usineUnTiers;
	private Image usineDeuxTiers;
	private Image usinePleine;
	
	public VisuelUsine(String usineVide, String usineUnTiers, String usineDeuxTiers, String usinePleine) {
		super();
		try {
			this.usineVide 		= ImageIO.read(new File(usineVide));
			this.usineUnTiers 	= ImageIO.read(new File(usineUnTiers));
			this.usineDeuxTiers = ImageIO.read(new File(usineDeuxTiers));
			this.usinePleine 	= ImageIO.read(new File(usinePleine));
		}catch (IOException e) {
			System.out.println(Constante.ERROR0001B);
		}
	}

	public void setStatut(int changementStatut) {
		this.statut = changementStatut;
	}

	public Image getImage() {
		switch (statut) {
		case 1:
			return usineUnTiers;
		case 2:
			return usineDeuxTiers;
		case 3:
			return usinePleine;
		case 0:
		default:
			return usineVide;
		}
	}
	
}
