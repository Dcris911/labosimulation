package com.util;

import java.awt.Dimension;

public class Constante {

	/** Configuration Application G�n�ral */
	public static final int 	DELAI 							= 100;
	public static final int 	INCREMENTEUR 					= 10;
	public static final int 	NB_DEPLACEMENT_PAR_TOUR 		= 10;	
	public static final int 	LIMITE_COMPOSANTE_PAR_CHEMIN 	= 5;
	public static final float 	TAUX_RALENTISSEMENT_USINE_METAL = 1.5f;
	public static final boolean ACTIVER_AJUSTEMENT_PRODUCTION 	= true;
	
	/** Dimensions */
	public static final Dimension DIMENSION_FENETRE_PRINCIPALE 	= new Dimension(700, 700);
	public static final Dimension DIMENSION_FENETRE_STRATEGIE 	= new Dimension(250, 100);
	public static final int TAILLE_USINE 		= 32;
	public static final int TAILLE_COMPOSANTE 	= 32;
	
	/** Usine */
	public static final int NOMBRE_ETAT 	= 4;
	public static final int ETAT_VIDE 		= 0;
	public static final int ETAT_UN_TIERS 	= 1;
	public static final int ETAT_DEUX_TIERS = 2;
	public static final int ETAT_PLEIN 		= 3;

	/** Strategie Vente */
	public static final int SEED_MAXIMAL_RANDOM = 275;
	public static final int NB_TOUR_ATTENTE 	= 250;
	
	public static final String STRATEGY_ALEATOIRE 	= "Vente Aleatoire";
	public static final String STRATEGY_FIXE 		= "Vente Fixe";
	public static final String STRATEGY_DEFAUT 		= "Strategie par Defaut";

	/* Path XML */
	public static final String PATH_CONFIGURATION_RELATIVE 	= "src\\ressources\\configuration.xml";
	public static final String PATH_TEST 					= "src\\ressources\\configurationTest.xml";
	public static final String PATH_CONFIGURATION 			= "C:\\Users\\Portable\\Desktop\\ETS\\Session3\\Log\\Workspace\\Lab01\\src\\ressources\\configuration.xml";
	
	/* Composante */
	public static final String VENTE_AVION = "Vente de l'avion numero : ";
	public static final String AJUSTEMENT_PRODUCTION = "Ajustement fait production. Temps Fabrication chang� pour : ";
	
	/* XML */
	public static final String METAL	= "metal";
	public static final String MOTEUR   = "moteur";
	public static final String AILE		= "aile";
	public static final String AVION	= "avion";
	
	public static final String NOM_ENTREPOT		= "entrepot";
	public static final String NOM_USINE_AILE   = "usine-aile";
	public static final String NOM_USINE_AVION  = "usine-assemblage";
	public static final String NOM_USINE_METAL  = "usine-matiere";
	public static final String NOM_USINE_MOTEUR = "usine-moteur";

	public static final String VIDE			= "vide";
	public static final String UNTIERS		= "un-tiers";
	public static final String DEUXTIERS	= "deux-tiers";
	public static final String PLEIN		= "plein";	

	/* Path des diff�rentes images */
    public static final String PATH_METAL  		= "src\\ressources\\metal.png";
    public static final String PATH_MOTEUR 		= "src\\ressources\\moteur.png";
    public static final String PATH_AILE   		= "src\\ressources\\aile.png";
    public static final String PATH_AVION  		= "src\\ressources\\avion.png";

    public static final String PATH_UMETAL_1 	= "src\\ressources\\UMP0%.png";
	public static final String PATH_UMETAL_2 	= "src\\ressources\\UMP33%.png";
	public static final String PATH_UMETAL_3 	= "src\\ressources\\UMP66%.png";
	public static final String PATH_UMETAL_4 	= "src\\ressources\\UMP100%.png";
    public static final String PATH_UMOTEUR_1	= "src\\ressources\\UM0%.png";
	public static final String PATH_UMOTEUR_2	= "src\\ressources\\UM33%.png";
	public static final String PATH_UMOTEUR_3	= "src\\ressources\\UM66%.png";
	public static final String PATH_UMOTEUR_4	= "src\\ressources\\UM100%.png";
    public static final String PATH_UAILE_1 	= "src\\ressources\\UT0%.png";
	public static final String PATH_UAILE_2 	= "src\\ressources\\UT33%.png";
	public static final String PATH_UAILE_3		= "src\\ressources\\UT66%.png";
	public static final String PATH_UAILE_4 	= "src\\ressources\\UT100%.png";
    public static final String PATH_UAVION_1 	= "src\\ressources\\UA0%.png";
	public static final String PATH_UAVION_2 	= "src\\ressources\\UA33%.png";
	public static final String PATH_UAVION_3 	= "src\\ressources\\UA66%.png";
	public static final String PATH_UAVION_4 	= "src\\ressources\\UA100%.png";
	public static final String PATH_ENTREPOT_1 	= "src\\ressources\\E0%.png";
	public static final String PATH_ENTREPOT_2 	= "src\\ressources\\E33%.png";
	public static final String PATH_ENTREPOT_3 	= "src\\ressources\\E66%.png";
	public static final String PATH_ENTREPOT_4 	= "src\\ressources\\E100%.png";
	
	public static final String EMPTY_STRING		= "";
	
	/* Liste des Erreurs possibles */
	public static final String ERROR0001A = "ERROR0001A :Incapable obtenir image des composantes";
	public static final String ERROR0001B = "ERROR0001B : Incapable obtenir image des usines";
	public static final String ERROR0002 = "ERROR0002 : Probleme creation Usine";
	public static final String ERROR0003 = "ERROR0003 : Probleme avec le transfert entre le chemin et l'usine d'arrive, un composant d'un type non-voulu a �t� envoy� � l'usine d'arriv�";
	public static final String ERROR0004 = "ERROR0004 : Aucun entrepot dans la liste pour les strategies";
	
	/* Swing */
	public static final String TITRE_FENETRE_FENETRE_PRINCIPALE = "Laboratoire 1 : LOG121 - Simulation";
	public static final String TITRE_FENETRE_FENETRE_STRATEGIE 	= "S�lectionnez votre strat�gie de vente";

	public static final String NOM_PROPRIETE_CHANGER			= "TEST";
	
	public static final String MENU_FICHIER_TITRE 				= "Fichier";
	public static final String MENU_FICHIER_CHARGER 			= "Charger";
	public static final String MENU_FICHIER_QUITTER 			= "Quitter";
	public static final String MENU_SIMULATION_TITRE 			= "Simulation";
	public static final String MENU_SIMULATION_CHOISIR 			= "Choisir";
	public static final String MENU_AIDE_TITRE 					= "Aide";
	public static final String MENU_AIDE_PROPOS 				= "� propos de...";

	public static final String MENU_SELECTION_XML 				= "S�lectionnez un fichier de configuration";
	public static final String MENU_EXTENSION_XML 				= ".xml";
	public static final String MENU_XML 						= "xml";

	public static final String BOUTON_CONFIRMER 				= "Confirmer";
	public static final String BOUTON_ANNULER					= "Annuler";
	
}
