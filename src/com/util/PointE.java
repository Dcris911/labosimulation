package com.util;

import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * Classe permetant d'avoir un point sous format Float et de retourner des attributs sous format float 
 *   au lieu de les avoir seulement sous format Int et Double
 * @author David Gilbert
 */
public class PointE extends Point2D.Float{

	private static final long serialVersionUID = -2283620724068113807L;

	public PointE() {
		super(0,0);
	}
	
	public PointE(int x, int y) {
		super(x,y);
	}
	
	public PointE(float x, float y) {
		super(x,y);
	}
	
	public PointE(Point p) {
		super(p.x,p.y);
	}

	/**
	 * D�place le point
	 * @param dx D�placement en X
	 * @param dy D�placement en Y
	 */
    public void translate(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

	/**
	 * D�place le point
	 * @param dx D�placement en X
	 * @param dy D�placement en Y
	 */
    public void translate(float dx, float dy) {
        this.x += dx;
        this.y += dy;
    }
    
    /**
     * @return La position en X sous format Float
     */
    public float getXf() {
    	return x;
    }
    
    /**
     * @return La position en Y sous format Float
     */
    public float getYf() {
    	return y;
    }
    
    /**
     * @return La position en X sous format Int
     */
    public int getXi() {
    	return (int)Math.round(x);
    }
    
    /**
     * @return La position en Y sous format Int
     */
    public int getYi() {
    	return (int)Math.round(y);
    }
    
    /**
     * Cr�e le vecteur unitaire avec une coordonn�e en x et en y
     * @param px Coordonn�e en X
     * @param py Coorfonn�e en Y
     * @return Un point symbolisant le vecteur unitaire
     */
    public static PointE vecteurUnitaire(int px, int py) {
    	float x,y;
    	if(px == 0 && py == 0) {
    		x = 0f;
    		y = 0f;
    	}else {
    		float pxf = px;
    		float pyf = py;
    		float norme = (float)Math.sqrt( (Math.pow(px, 2)+Math.pow(py, 2)));
    		x = (pxf/norme);
    		y = (pyf/norme);
    	}
    	return new PointE(x, y);
    }
}
